import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Persons from '@/components/Persons'
import PersonAdd from '@/components/PersonAdd'
import PersonShow from '@/components/PersonShow'
import PersonEdit from '@/components/PersonEdit'
import Draws from '@/components/Draws'
import DrawAdd from '@/components/DrawAdd'
import DrawShow from '@/components/DrawShow'
import TransAdd from '@/components/TransAdd'
import Trans from '@/components/Trans'
import TransShow from '@/components/TransShow'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/person',
      name: 'Persons',
      component: Persons
    },
    {
      path: '/person/show/:id',
      name: 'PersonShow',
      component: PersonShow
    },
    {
      path: '/person/add',
      name: 'PersonAdd',
      component: PersonAdd
    },
    {
      path: '/person/edit',
      name: 'PersonEdit',
      component: PersonEdit
    },
    {
      path: '/donor',
      component: Persons,
      props: {mode: 'donor', pageTitle: 'Доноры'}
    },
    {
      path: '/donor/show/:id',
      component: PersonShow,
      props: { mode: 'donor'},
      children: [
        { path: '/', component: Draws, props: { mode: 'donordraws'}}
      ]
    },
    {
      path: '/recipient',
      component: Persons,
      props: {mode: 'recipient', pageTitle: 'Реципиенты'}
    },
    {
      path: '/recipient/show/:id',
      component: PersonShow,
      props: { mode: 'recipient'},      
      children: [
        { path: '/', component: Trans, props: { mode: 'recipienttrans'}}
      ]
    },
    {
      path: '/draw',
      name: 'Draws',
      component: Draws
    },
    {
      path: '/draw/show/:id',
      name: 'DrawShow',
      component: DrawShow
    },
    {
      path: '/draw/add',
      name: 'DrawAdd',
      component: DrawAdd
    },
    {
      path: '/trans',
      name: 'Trans',
      component: Trans
    },
    {
      path: '/trans/show/:id',
      name: 'TransShow',
      component: TransShow
    },
    {
      path: '/trans/add',
      name: 'TransAdd',
      component: TransAdd
    }
  ]
})

