import Api from '@/services/Api'

export default {
  fetchTrans (queryObj) {
    return Api().get('api/trans', { params: queryObj });
  },
  addTrans(params) {
    return Api().post('api/trans', params);
  },
  getTrans(params) {
    return Api().get('api/trans/' + params.id);
  },
  updateTrans(params) {
    return Api().put('api/trans/' + params.id, params);
  }
}
