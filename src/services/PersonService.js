import Api from '@/services/Api'

export default {
  fetchPersons (queryObj) {
    return Api().get('api/person', {params: queryObj});
  },
  addPerson (params) {
    return Api().post('api/person', params);
  },
  getPerson (params) {
    return Api().get('api/person/' + params.id);
  },
  updatePerson (params) {
    return Api().put('api/person/' + params.id, params);
  }
}
