import Api from '@/services/Api'

export default {
  fetchDraws (queryObj) {
    return Api().get('api/draw', {params: queryObj});
  },
  addDraw(params) {
    return Api().post('api/draw', params);
  },
  getDraw(params) {
    return Api().get('api/draw/' + params.id);
  },
  updateDraw(params) {
    return Api().put('api/draw/' + params.id, params);
  }
}
