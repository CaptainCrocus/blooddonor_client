// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import Tools from '@/lib/Tools'
import ErrorBoard from '@/components/ErrorBoard'

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.prototype.$tools = Tools;
Vue.component('error-board', ErrorBoard);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
