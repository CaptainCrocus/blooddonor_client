import Api from '@/services/Api'

export default {
  fetchSources () {
    return Api().get('api/source')
  }
}
