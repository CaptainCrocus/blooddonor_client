import Api from '@/services/Api'

export default {
  fetchBloodTypes () {
    return Api().get('api/bloodtype')
  }
}
