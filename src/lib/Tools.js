export default {
  smartJoin(arr, separator){
    if(!separator) separator = ' ';
      return arr.filter(function(elt){
        return elt!==undefined && elt!==null && elt.toString().trim() !== '';
      }).join(separator);
  },
  formatDate(date) {
	  var dd = date.getDate();
	  if (dd < 10) dd = '0' + dd;

	  var mm = date.getMonth() + 1;
	  if (mm < 10) mm = '0' + mm;

	  var yyyy = date.getFullYear();

	  return dd + '.' + mm + '.' + yyyy;
  },
  formatTime(date) {
	  var hh = date.getHours();
	  if (hh < 10) hh = '0' + hh;

	  var mm = date.getMinutes();
	  if (mm < 10) mm = '0' + mm;

	  return hh + ':' + mm;
	}
}
