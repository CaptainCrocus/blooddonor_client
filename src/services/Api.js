import axios from 'axios'

export default() => {
  var instance = axios.create({
    baseURL: `http://localhost:5000`,
    withCredentials: true
  });

  instance.interceptors.response.use(function (response) {
    	if(undefined !== response.data.success && !response.data.success && 
    		undefined !== response.data.redirect) {
    		window.location = window.location.origin + response.data.redirect;
    	}
    return response;
  }, function (error) {
    return Promise.reject(error);
  });

  return instance
}
